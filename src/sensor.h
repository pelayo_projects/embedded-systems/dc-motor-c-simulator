#ifndef SENSOR_H
#define SENSOR_H
#include"variables_compartidas.h"
#include<pthread.h>


class sensor
{

    float ganancia;

    float resolucion;
    //sensor necesita acceder a var comp
    variables_compartidas *var;
    //necesito mutex porque regulador del motor y del crucero accederan al sensor
    pthread_mutex_t mut_sensor;

public:
    sensor(float,float,variables_compartidas*);

    float SalidaSensor();
    float get_Ganancia();
    float get_Resolucion();
};

#endif // SENSOR_H
