#include "hilos.h"
#include "pthread.h"
#include "fdt.h"
#include <unistd.h>
#include <iostream>
using namespace std;
#include <time.h>
#include"regulador.h"
#include"planta.h"
#include"conversor.h"
#include<math.h>


//Sobrecargo el operador + para poder sumar timespecs
timespec  operator+(const timespec& s1,const timespec& s2){
    timespec s;
    s.tv_sec=s1.tv_sec+s2.tv_sec;
    s.tv_nsec=s1.tv_nsec+s2.tv_nsec;
    if(s.tv_nsec>=1E9){ //Si me paso de 1e9 nanoseg, sumo 1 seg
        s.tv_sec++;
        s.tv_nsec-=1E9;
    }
    return s;
}


// Funcion que recibe los parametros del regulador e inicia el bucle para leer el valor del sensor y devuelve la uk
void* HiloRegulador(void *p){


    regulador *reg=(regulador*)p;
    reg->bucle_regulador();


}


void* HiloPlanta(void *p){

    planta *plant=(planta*)p;
    plant->bucle_planta();

}

void* HiloConversor(void *p){

    conversor *conv=(conversor*)p;

    //utilizo wrapping para ejecutar el bucle de conversion

    conv->bucle_conversion();





}
