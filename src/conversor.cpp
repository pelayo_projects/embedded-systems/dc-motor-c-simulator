#include "conversor.h"
#include<math.h>
#include<cstdlib>



conversor::conversor(float vmax,float vmin,int res, int n,sensores * s_m_c){
    Vmax=vmax;
    Vmin=vmin;
    resolucion=res;
    n_canales=n;
    sensor_m=s_m_c->sensor_m; //VARIABLES COMPARTIDAS DEL MOTOR
    sensor_c=s_m_c->sensor_c; //VARIABLES CRUCERO

    pthread_mutex_init(&mut_read,nullptr);
    pthread_mutex_init(&mut_conversion,nullptr);
    pthread_cond_init(&condicion_START,nullptr);
    pthread_cond_init(&condicion_DONE,nullptr);
}

void conversor::read(int canal, uint16 * dato){

    //Como varios hilos pueden utilizar el conversor, utilizo mutex

    pthread_mutex_lock(&mut_read);

    // Se ponen los campos requeridos a sus valores.
    CSR.ERROR=0;

    CSR.IE=1;

    CSR.DONE=0;

    CSR.CANAL=canal;

    //Para que comienze la cnversion es necesario poner a 1 el bit de start

    CSR.START=1;

    // señalo al bucle de conversio para que inicie
    pthread_cond_signal(&condicion_START);

    pthread_mutex_lock(&mut_conversion);

    while(CSR.DONE==0){ //espero a que termine

        pthread_cond_wait(&condicion_DONE,&mut_conversion); //señalo
    }

    if (CSR.ERROR==0)
        *dato=DR.VALOR; // si no hay error, cargo el resultado en el Data Register, en la posicion de memoria
    //indicada por el dato

    pthread_mutex_unlock(&mut_conversion);
    pthread_mutex_unlock(&mut_read);



}

void conversor::convertir(){ //metodo que ejecutara la conversion
    int conversion;

    if(CSR.CANAL==0){

        //Hago una interpolacion para convertir la salida del sensor

        conversion=(int)((pow(2,resolucion)-1)/2+sensor_m->SalidaSensor()*(pow(2,resolucion)-1)/(Vmax-Vmin));



        if(conversion<0)
            DR.SIGNO=1;
        else
            DR.SIGNO=0;


        DR.VALOR=conversion;

    }
    else{
        conversion=(int)((pow(2,resolucion)-1)/2+sensor_c->SalidaSensor()*(pow(2,resolucion)-1)/(Vmax-Vmin));

        if(conversion<0)
            DR.SIGNO=1;
        else
           DR.SIGNO=0;


        DR.VALOR=conversion;
    }
}
void conversor::bucle_conversion(){

    while(1){ //Quiero que siempre se este convirtiendo

        //cerramos el mutex de conversion

        pthread_mutex_lock(&mut_conversion);

        while(CSR.START==0){
            pthread_cond_wait(&condicion_START,&mut_conversion);

        }
        CSR.START=0;//pongo start a 0 porque voy a lanzar la conversion

        convertir(); //Realizo la conversion

        CSR.DONE=1;//indico que termino la conversion

        pthread_cond_signal(&condicion_DONE); //Mandamos señal de que done esta a 1

        pthread_mutex_unlock(&mut_conversion);
    }
}

float conversor:: get_VMAX(){
    return Vmax;
}
float conversor::get_Vmin(){
    return Vmin;

}
float conversor::get_Resolucion(){
    return resolucion;
}
