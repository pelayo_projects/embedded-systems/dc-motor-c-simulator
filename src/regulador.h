#ifndef REGULADOR_H
#define REGULADOR_H
#include "fdt.h"
#include"hilos.h"
#include"conversor.h"
#include"variables_compartidas.h"
#include <pthread.h>


class regulador
{
    fdt *funcion;  //puntero a una fdt
    float Kp;      //valor de la ganancia del regulador
    float T_regulador;       //periodo del regulador
    variables_compartidas *v;
    conversor *conv;
    pthread_mutex_t mut_kp; //necesario porque tambien puedo cambiar la kp desde la pantalla

public:
    regulador(fdt *, float, float,variables_compartidas*,conversor*);
    float CalcularSalida(float);
    //SETTER
    void set_Kp(float kp);
    //GETTERS
    float get_Kp();
    float get_T_regulador();
    //BUCLE
    void bucle_regulador();

};


#endif // REGULADOR_H
