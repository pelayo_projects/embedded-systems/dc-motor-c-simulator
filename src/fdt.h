#ifndef FDT_H
#define FDT_H


class fdt
{
    float *xk; //entrada
    float *yk; //salida
    float *numerador; //Coeficientes del numerador
    float *denominador; //Coeficientes del denominador
    int n; //Grado numerador
    int m; //Grado denominador

    float ProductoEscalar();//Necesaria para realizar el producto de
    void ActualizaTabla();



public:

    fdt(float *a, float *b, int n, int m);//constructor de la clase fdt

    float CalcularSalida(float ek);//Metodo para calcular la salida
    //a partir de la entrada ek

};



#endif // FDT_H
