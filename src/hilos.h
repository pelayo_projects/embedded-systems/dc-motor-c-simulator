#ifndef HILOS_H
#define HILOS_H
#include "fdt.h"
#include <semaphore.h>
#include "variables_compartidas.h"

#include"planta.h"
#include"conversor.h"
#include"regulador.h"


/*struct datos_hilo{
    regulador *reg;
    planta    *plant;
    variables_compartidas *v;
    conversor *conv;

};*/

//sobrecargo + para sumar timespecs
timespec  operator+(const timespec& s1,const timespec& s2);

//3 hilos

void* HiloRegulador(void *);
void* HiloPlanta(void *);
void* HiloConversor(void *);

#endif // HILOS_H
