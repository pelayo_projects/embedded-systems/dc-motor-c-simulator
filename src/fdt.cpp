#include "fdt.h"



fdt::fdt(float *a, float *b, int n1, int m1){

    //Memoria necesaria para almacenar las tablas
    numerador   = new float[n1];
    denominador = new float[m1];
    yk = new float[n1];
    xk = new float[m1];
     n = n1;
     m = m1;

     //paso parametros del numerador
    for ( int i=0; i<n ; i++ ){

        numerador[i]=a[i];
        yk[i]=0;
    }
    //paso parametros del denominador
    for ( int i=0; i<m ; i++ ){

        denominador[i]=b[i];
        xk[i]=0;

    }
}



void fdt::ActualizaTabla(){ //Voy actualizando la lista de valores de la tabla para que bajen una posicion


    for(int i = n-1 ; i>0 ; i--){

       xk[i]=xk[i-1];
       yk[i]=yk[i-1];

    }


}

float fdt :: ProductoEscalar(){//Metodo para realizar el producto escalar entre los coeficientes de la fdt
    float producto=0; //variable donde almacenamos producto escalar
      for(int i=0;i<n;i++)
          producto+=numerador[i]*xk[i];
      for(int i=1;i<m;i++)
          producto-=denominador[i]*yk[i];
      producto/=denominador[0];
      return producto;
}


float fdt::CalcularSalida(float entrada){

    ActualizaTabla();


    xk[0]=entrada;          //actualizo entrada
    yk[0]=ProductoEscalar();    //calculo salida
    return yk[0];     //devuelvo salida


}

