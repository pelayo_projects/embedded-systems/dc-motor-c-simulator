#ifndef CONVERSOR_H
#define CONVERSOR_H
#include<pthread.h>
#include<variables_compartidas.h>
#include"sensor.h"

struct sensores{
    sensor *sensor_m;
    sensor *sensor_c;
};

typedef unsigned char uint8;

typedef unsigned int uint16;

// Creo un registro DR para simular un valor de 12 bits con signo.
//Para crearlo realemnte necesitaria utilizar mascaras de bits y hacer una
//interpolacion entre el valor mas alto 2047 (011111111111) y -2047 (111111111111)
struct registroDR{
    uint16  SIGNO:1,
            NOUSE:3,
            VALOR:12;
};

//Creo un mapa de bits para emular el registro CSR, aprovecho que un char tiene 8 bits de tamaño.
struct registoCSR{
    uint8   ERROR:2,
            IE:1,
            DONE:1,
            CANAL:2,
            NOUSE:1,
            START:1;

};

class conversor
{
    registoCSR CSR;
    registroDR DR;

    //Para exclusion entre hilos
    pthread_mutex_t mut_read;
    pthread_mutex_t mut_conversion;
    pthread_cond_t condicion_START;
    pthread_cond_t condicion_DONE;
    //atributos
    float Vmin, Vmax;
    int resolucion;
    int n_canales;



public:
    //pongo los sensores publicos porque el regulador necesita acceder a ellos
    sensor *sensor_m,*sensor_c;

    conversor(float, float, int, int,sensores*);
    void read(int, uint16*);// funcion que llama al regulador para empezar la conversion
    void convertir();//Implementamos conversion y escribimos resutado en DR
    void bucle_conversion(); //Estara continuamente convirtiendo

    //getters

    float get_VMAX();
    float get_Vmin();
    float get_Resolucion();



};

#endif // CONVERSOR_H
