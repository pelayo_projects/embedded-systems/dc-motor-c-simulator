#ifndef VARIABLES_COMPARTIDAS_H
#define VARIABLES_COMPARTIDAS_H
#include <pthread.h>

class variables_compartidas
{
    float Referencia;
    float uk;
    float yk[5];    //Tabla de 5 valores de la que luego extraere para mostrar en pantalla
    pthread_mutex_t mutvar; // acceso exclusivo a cada variable


public:

    variables_compartidas();
    /*SETTERS Y GETTERS*/
    void set_Referencia(float);
    float get_Referencia();
    void set_uk(float);
    float get_uk();
    void set_yk(float);
    float get_yk(int);

};

#endif // VARIABLES_COMPARTIDAS_H
