#include "hilos.h"
#include "pthread.h"
#include <unistd.h>
#include <iostream>
using namespace std;
#include <time.h>
#include"regulador.h"
#include"conversor.h"
#include<math.h>

regulador::regulador(fdt *f1, float k, float tiempo,variables_compartidas*var,conversor*convers)
{
    funcion=f1;
    Kp=k;
    T_regulador=tiempo;//periodo reg
    v=var; //necesita  la referencia
    conv=convers; //necestio convertir la señal del sensor
    pthread_mutex_init(&mut_kp,NULL);
}



//SETTER
//Necesitamos mutex ya que kp se puede acceder desde motor y crucero
void regulador::set_Kp(float x){
    pthread_mutex_lock(&mut_kp);
    Kp=x;
    pthread_mutex_unlock(&mut_kp);
}


float regulador::get_Kp(){
    float aux;
    pthread_mutex_lock(&mut_kp);
    aux=Kp;
    pthread_mutex_unlock(&mut_kp);
    return aux;
}


float regulador::get_T_regulador(){
    return T_regulador;
}



float regulador::CalcularSalida(float e){
    float aux;
    pthread_mutex_lock(&mut_kp);
    aux=Kp*funcion->CalcularSalida(e);
    pthread_mutex_unlock(&mut_kp);
    return aux;
}

void regulador::bucle_regulador(){

    uint16 salida_conversor;
    timespec t1,periodo;
    //paso el periodo a milisegundos
    periodo.tv_sec=0;
    periodo.tv_nsec=this->get_T_regulador()*1E6;
    while(periodo.tv_nsec>=1E9){
        periodo.tv_sec++;
        periodo.tv_nsec-=1E9;
    }


    float ek=0;
    float salida;

    //utilizo clock_gettime para obtener el tiempo actual real y guardarlo en t1
    clock_gettime(CLOCK_REALTIME,&t1);
    while(1){ // Bucle de hilo regulador

        t1=t1+periodo;
        if(this->get_T_regulador()==50)
            this->conv->read(0,&salida_conversor); //para detectar si es el crucero o el motor, compruebo su periodo
        else                                        //y ejecuto la funcion read
            this->conv->read(1,&salida_conversor);

        //Calculo la salida del sensor y la convierto
        //Realizo una interpolacion
         salida=(this->conv->get_Vmin()+(salida_conversor-0)*(this->conv->get_VMAX()-this->conv->get_Vmin())/(pow(2,this->conv->get_Resolucion())-1-0));

        //dependiendo de si es motor o crucero utilizo una resolucion y ganancia o otra.
        if(this->get_T_regulador()==50)
            salida*= (1/(this->conv->sensor_m->get_Resolucion()*this->conv->sensor_m->get_Ganancia()));
        else
            salida*= (1/(this->conv->sensor_c->get_Resolucion()*this->conv->sensor_c->get_Ganancia()));

        //despues de convertir la yk calculo el error

        ek=this->v->get_Referencia()-salida;

        //calculo nueva uk y guardo en variables compartidas

        this->v->set_uk((this->CalcularSalida(ek)));
        //cout<<k<<"\t\t"<<ek<<"\t\t";

        //intervalo en tiempo real de t + periodo
        clock_nanosleep(CLOCK_REALTIME,TIMER_ABSTIME,&t1,NULL);
}
}

