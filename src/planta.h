#ifndef PLANTA_H
#define PLANTA_H
#include "fdt.h"
#include"variables_compartidas.h"
#include"conversor.h"
#include"hilos.h"
#include<time.h>

class planta
{
    fdt *funcion;
    float T_planta;
    variables_compartidas * v;

public:
    //Le paso variables compartidas y conversor para hacer wrapping

    planta(fdt*,float,variables_compartidas*);//constuctor por asignacion, sin herencia

    float CalcularSalida(float);
    float get_T_planta();
    void bucle_planta();

};

#endif // PLANTA_H
