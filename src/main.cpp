/*SISTEMAS DE TIEMPO REAL 2021 PELAYO LEGUINA LOPEZ*/



#include "pantalla.h"
#include <iostream>
#include <pthread.h>
#include"regulador.h"
#include"planta.h"
#include"conversor.h"
#include "fdt.h"
#include "hilos.h"

using namespace std;


#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);


    pthread_t h1,h2,h3,h4,h5;

 /*DEFINICION OBJETOS*/

    variables_compartidas var_motor;

    variables_compartidas var_crucero;

    sensor sensor_motor(500,0.001,&var_motor);

    sensor sensor_crucero(500,0.001,&var_crucero);

    sensores sensores_trabajo={&sensor_motor,&sensor_crucero};

    conversor conv(10,-10,12,2,&sensores_trabajo);


/*PLANTA*/

    float bpm[]={0,0.0005663,0.0005125};
    float apm[]={1,-1.73,0.7408};
    fdt funcionplanta(bpm,apm,3,3);
    planta plantamotor(&funcionplanta,25,&var_motor);



    float bpc[]={0,9.998*1E-6};
    float apc[]={1,-0.9995};
    fdt funcionplantacrucero(bpc,apc,2,2);
    planta plantacrucero(&funcionplantacrucero,10,&var_crucero);

/*REGULADOR*/

    float arm[]={1,0.82,-1,-0.82};
    float brm[]={414.1,-647.8,250.1,0};
    fdt funcionregulador(brm,arm,4,4);
    regulador regmotor(&funcionregulador,0.1,50,&var_motor,&conv);


    float arc[]={1,-0.9998};
    float brc[]={489.08,-488.5918};
    fdt funcionreguladorcrucero(brc,arc,2,2);
    regulador regcrucero(&funcionreguladorcrucero,1,20,&var_crucero,&conv);



/*ESTRUCTURA DATOS HILO PARA PASAR COMO PARAMETRO A LOS HILOS*/

    //SE HA ELIMINADO CON LA INTENCION DE HACER WRAPPING

    //datos_hilo HMotor={&regmotor,&plantamotor,&var_motor,&conv};

    //datos_hilo HCrucero={&regcrucero,&plantacrucero,&var_crucero,&conv};


/*PRIORIDADES DE HILOS*/

    /* Despues de haber calculado las prioridades de cada hilo,
     * utilizo la funcion pthread_attr_setschedpolicy para establecer
     * una planificacion FIFO como indica el enunciado. Ademas, creo
     * parametros sched para marcar las prioridades de cada hilo. Como
     * se calculo, al hilo regulador del motor se le da la prioridad mas baja
     * (ya que no hay hilo para la pantalla) y al hilo planta del crucero la
     * mas alta. Despues con pthread_attr_setschedparam añado esos parametros a
     * los atributos previamente definidos y despues e entroducene n cada hilo*/

    //ATRIBUTOS
    pthread_attr_t attr_h1,attr_h2,attr_h3,attr_h4;
    sched_param param_1,param_2,param_3,param_4;
    //POLITICA DE PLANIFICACION
    pthread_attr_setschedpolicy(&attr_h1,SCHED_FIFO);
    pthread_attr_setschedpolicy(&attr_h2,SCHED_FIFO);
    pthread_attr_setschedpolicy(&attr_h3,SCHED_FIFO);
    pthread_attr_setschedpolicy(&attr_h4,SCHED_FIFO);
    //PRIORIDADES PLANIFICACION
    param_1.sched_priority = 2;
    param_2.sched_priority = 3;
    param_3.sched_priority = 4;
    param_4.sched_priority = 5;

    pthread_attr_setschedparam(&attr_h1,&param_1);
    pthread_attr_setschedparam(&attr_h2,&param_2);
    pthread_attr_setschedparam(&attr_h3,&param_3);
    pthread_attr_setschedparam(&attr_h4,&param_4);


    /*CREACION HILOS*/
    //creo los hilos añadiendoles los atributos de prioridD yel objeto en cuestion como parametro de la funcion.
    pthread_create(&h1,&attr_h1,HiloRegulador,(void*)&regmotor);
    pthread_create(&h2,&attr_h2,HiloPlanta,(void*)&plantamotor);
    pthread_create(&h3,&attr_h3,HiloRegulador,(void*)&regcrucero);
    pthread_create(&h4,&attr_h4,HiloPlanta,(void*)&plantacrucero);
    pthread_create(&h5,NULL,HiloConversor,(void*)&conv);
    //Como en la planificacion el conversor lo he añadido a los reguladores aqui no lo tengo en cuenta

    /*PANTALLA*/
    //le paso una estructura con los objetos con los que va a interactuar.
    datosPantalla datos={&var_motor,&var_crucero,&regmotor,&regcrucero};
    Pantalla w(nullptr,&datos);
    w.show();
    return a.exec();
}
