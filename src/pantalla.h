#ifndef PANTALLA_H
#define PANTALLA_H

#include <QMainWindow>
#include"conversor.h"
#include"variables_compartidas.h"
#include"regulador.h"
#include"qcustomplot.h"
#include<time.h>
#include <QTimer>


QT_BEGIN_NAMESPACE
namespace Ui { class Pantalla; }
QT_END_NAMESPACE
struct datosPantalla{ //Meto los datos que voy utilizar para la pantalla
    variables_compartidas *var_m;
    variables_compartidas *var_c;
    regulador *regmotor;
    regulador *regcrucero;
};

class Pantalla : public QMainWindow
{
    Q_OBJECT

public:
    Pantalla(QWidget *parent = nullptr,datosPantalla *datos=nullptr);
    ~Pantalla();

private slots:
    void on_pushButton_captura_m_clicked();

    void on_pushButton_captura_c_clicked();

    void on_dial_kp_c_sliderMoved(int position);

    void on_dial_r_c_sliderMoved(int position);

    void on_dial_kp_m_sliderMoved(int position);

    void on_dial_r_m_sliderMoved(int position);

    void setupRealtimeDataDemo(QCustomPlot *customPlot);
    void realtimeDataSlot();

private:
    Ui::Pantalla *ui;
    datosPantalla *datos_pantalla;
    QTimer dataTimer;
};
#endif // PANTALLA_H
