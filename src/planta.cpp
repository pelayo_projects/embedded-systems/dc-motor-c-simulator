#include "planta.h"

//Paso una fdt por referencia y utilizo su metodo CalcularSalida para obtener el valor de la Yk

planta::planta(fdt* planta,float t,variables_compartidas*vars){

    funcion=planta;
    v=vars;

    T_planta=t;
}


float planta::CalcularSalida(float uk){

    float Salida = funcion->CalcularSalida(uk);

    return Salida;
}


float planta::get_T_planta(){
    return T_planta;
}

void planta::bucle_planta(){

    timespec t1,periodo;
    periodo.tv_sec=0;
    periodo.tv_nsec=this->get_T_planta()*1E6;
    while(periodo.tv_nsec>=1E9){
        periodo.tv_sec++;
        periodo.tv_nsec-=1E9;
    }
    clock_gettime(CLOCK_REALTIME,&t1);
    while(1){

        t1=t1+periodo;

        //La planta se encarga de obtener el nuevo valor de uk y calcular la salida del sistema yk,
        //guardandola en variables compartidas

        this->v->set_yk(this->CalcularSalida(this->v->get_uk()));
        //cout<<datos->v->get_uk()<<"\t\t"<<datos->v->get_yk(0)<<endl;


        clock_nanosleep(CLOCK_REALTIME,TIMER_ABSTIME,&t1,NULL);

    }
}
