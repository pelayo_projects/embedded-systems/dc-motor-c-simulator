#include "pantalla.h"
#include "ui_pantalla.h"


Pantalla::Pantalla(QWidget *parent,datosPantalla *datos)
    : QMainWindow(parent)
    , ui(new Ui::Pantalla)
{
    ui->setupUi(this);

    ;
    /*VALORES INICIALES PANTALLA*/
    datos_pantalla=datos;
    ui->progressBar_m->setValue(0);
    ui->progressBar_c->setValue(0);
    ui->lcdNumber_r_m->display(datos_pantalla->var_m->get_Referencia());
    ui->lcdNumber_r_c->display(datos_pantalla->var_c->get_Referencia());
    ui->lcdNumber_kp_m->display(datos_pantalla->regmotor->get_Kp());
    ui->lcdNumber_kp_c->display(datos_pantalla->regcrucero->get_Kp());
    ui->dial_r_c->setValue(int(datos_pantalla->var_c->get_Referencia()*10.0));
    ui->dial_r_m->setValue(int(datos_pantalla->var_m->get_Referencia()*10.0));
    ui->dial_kp_c->setValue(int(datos_pantalla->regcrucero->get_Kp()*10.0));
    ui->dial_kp_m->setValue(int(datos_pantalla->regmotor->get_Kp()*10.0));


    setupRealtimeDataDemo(ui->plot_m);
    setupRealtimeDataDemo(ui->plot_c);

}

Pantalla::~Pantalla()
{
    delete ui;
}

/* BOTON CAPTURAR YK */
void Pantalla::on_pushButton_captura_m_clicked()
{
    QString text;

    ui->lineEdit_Yk0_m->setText(text.setNum(datos_pantalla->var_m->get_yk(0)));
    ui->lineEdit_Yk1_m->setText(text.setNum(datos_pantalla->var_m->get_yk(1)));
    ui->lineEdit_Yk2_m->setText(text.setNum(datos_pantalla->var_m->get_yk(2)));
    ui->lineEdit_Yk3_m->setText(text.setNum(datos_pantalla->var_m->get_yk(3)));
    ui->lineEdit_Yk4_m->setText(text.setNum(datos_pantalla->var_m->get_yk(4)));
}

/*BOTON CAPUTRAR YK CRUCERO*/
void Pantalla::on_pushButton_captura_c_clicked()
{
    QString text;

    ui->lineEdit_Yk0_c->setText(text.setNum(datos_pantalla->var_c->get_yk(0)));
    ui->lineEdit_Yk1_c->setText(text.setNum(datos_pantalla->var_c->get_yk(1)));
    ui->lineEdit_Yk2_c->setText(text.setNum(datos_pantalla->var_c->get_yk(2)));
    ui->lineEdit_Yk3_c->setText(text.setNum(datos_pantalla->var_c->get_yk(3)));
    ui->lineEdit_Yk4_c->setText(text.setNum(datos_pantalla->var_c->get_yk(4)));
}

/*Configuracion diales*/

//Divido la posicion / 10 y convierto en float porque le valor de los diales
//son numeros enteros, por lo tanto pongo el rango del dial entre 0 y 100 y despues divido
//pasandolo a numero real.
void Pantalla::on_dial_kp_c_sliderMoved(int position)
{
    QString text;
    datos_pantalla->regcrucero->set_Kp(float(position/10.0));
    ui->lcdNumber_kp_c->display(position/10.0);
}

void Pantalla::on_dial_r_c_sliderMoved(int position)
{
    QString text;
    datos_pantalla->var_c->set_Referencia(float(position/10.0));
    ui->lcdNumber_r_c->display(position/10.0);
}

void Pantalla::on_dial_kp_m_sliderMoved(int position)
{
    QString text;
    datos_pantalla->regmotor->set_Kp(float(position/100.0));
    ui->lcdNumber_kp_m->display(position/100.0);
}

void Pantalla::on_dial_r_m_sliderMoved(int position)
{
    QString text;
    datos_pantalla->var_m->set_Referencia(float(position/10.0));
    ui->lcdNumber_r_m->display(position/10.0);
}


void Pantalla::setupRealtimeDataDemo(QCustomPlot *customPlot)
{
    customPlot->setNotAntialiasedElements(QCP::aeAll);
    QFont font;
    font.setStyleStrategy(QFont::NoAntialias);
    customPlot->xAxis->setTickLabelFont(font);
    customPlot->yAxis->setTickLabelFont(font);
    customPlot->legend->setFont(font);

      customPlot->addGraph(); // blue line
      customPlot->graph(0)->setPen(QPen(Qt::blue));
      customPlot->graph(0)->setBrush(QBrush(QColor(240, 255, 200)));
      customPlot->graph(0)->setAntialiasedFill(false);
      customPlot->addGraph(); // red line
      customPlot->graph(1)->setPen(QPen(Qt::red));
      customPlot->graph(0)->setChannelFillGraph(customPlot->graph(1));

      customPlot->yAxis->setRange(0,1);

      // make left and bottom axes transfer their ranges to right and top axes:
      connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
      connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));

      // setup a timer that repeatedly calls MainWindow::realtimeDataSlot:
      connect(&dataTimer, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));
      dataTimer.start(0); // Interval 0 means to refresh as fast as possible
}


void Pantalla::realtimeDataSlot()
{
    static QTime time(QTime::currentTime());
    // calculate two new data points:
    double key = time.elapsed()/1000.0; // time elapsed since start of demo, in seconds
    static double lastPointKey = 0;
    if (key-lastPointKey > 0.002) // at most add point every 2 ms
    {
      // add data to lines:
      ui->plot_m->graph(0)->addData(key,datos_pantalla->var_m->get_Referencia());
      ui->plot_m->graph(1)->addData(key,datos_pantalla->var_m->get_yk(0));
      ui->plot_c->graph(0)->addData(key,datos_pantalla->var_c->get_Referencia());
      ui->plot_c->graph(1)->addData(key,datos_pantalla->var_c->get_yk(0));
      // rescale value (vertical) axis to fit the current data:
      ui->plot_m->graph(0)->rescaleValueAxis();
      ui->plot_m->graph(1)->rescaleValueAxis(true);
      ui->plot_c->graph(0)->rescaleValueAxis();
      ui->plot_c->graph(1)->rescaleValueAxis(true);
      lastPointKey = key;
    }
    // make key axis range scroll with the data (at a constant range size of 8):
    ui->plot_m->xAxis->setRange(key, 8, Qt::AlignRight);
    ui->plot_m ->replot();
    //Creo unas barras que simulan el grado de acierto del sistema (si e = 0, barra al 100%)

    ui->progressBar_m->setValue((datos_pantalla->var_m->get_yk(0)*100)/(datos_pantalla->var_m->get_Referencia()));
    ui->progressBar_c->setValue((datos_pantalla->var_c->get_yk(0)*100)/(datos_pantalla->var_c->get_Referencia()));
    ui->plot_c->xAxis->setRange(key, 8, Qt::AlignRight);
    ui->plot_c ->replot();
}


