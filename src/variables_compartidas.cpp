#include "variables_compartidas.h"

/*CONSTRUCTOR*/

variables_compartidas::variables_compartidas()
{
    Referencia=1; //Inicio con referencia = 1
    //Pongo a 0 las demas variables
    uk=0;
    for(int i=0;i<5;i++)
        yk[i]=0;
    pthread_mutex_init(&mutvar,NULL);
}

/*DEFINO SETTERS*/

//es necesario utilizar elementos de exclusion como el mutex,
//ya que tanto el motor como el crucero, asi como la planta y el regulador
//necesitan acceder a variables compartidas y podrian producirse bloqueos.
void variables_compartidas::set_yk(float y){
    pthread_mutex_lock(&mutvar); //actualizo yk y escribo yk0
    for(int i=4;i>0;i--)
        yk[i]=yk[i-1];
    yk[0]=y;
    pthread_mutex_unlock(&mutvar);
}

void variables_compartidas::set_uk(float u){
    pthread_mutex_lock(&mutvar);
    uk=u;
    pthread_mutex_unlock(&mutvar);
}


void variables_compartidas::set_Referencia(float r){
    pthread_mutex_lock(&mutvar);
    Referencia=r;
    pthread_mutex_unlock(&mutvar);
}

/*DEFINO GETTERS*/

float variables_compartidas::get_uk(){
    float aux;
    pthread_mutex_lock(&mutvar);
    aux=uk;
    pthread_mutex_unlock(&mutvar);
    return aux;
}

float variables_compartidas::get_yk(int i){
    float aux;
    pthread_mutex_lock(&mutvar);
    aux=yk[i];
    pthread_mutex_unlock(&mutvar);
    return aux;
}




float variables_compartidas::get_Referencia(){
    float aux;
    pthread_mutex_lock(&mutvar);
    aux=Referencia;
    pthread_mutex_unlock(&mutvar);
    return aux;
}




