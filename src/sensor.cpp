#include "sensor.h"

sensor::sensor(float g ,float res ,variables_compartidas *v)
{

    ganancia=g;

    resolucion=res;

    var=v;

    pthread_mutex_init(&mut_sensor,nullptr);

}

float sensor::SalidaSensor(){// calculo la salida del sensor ultiplicando la yk por la ganancia y la resolcuion


    float aux;
    //como accedo a variables compartidas, necesito utilizar exclusion.
    pthread_mutex_lock(&mut_sensor);

    aux= var->get_yk(0)*ganancia*resolucion;

    pthread_mutex_unlock(&mut_sensor);

    return aux;

}

/*GETTER GANANCIA*/
float sensor::get_Ganancia(){

    float aux;

    pthread_mutex_lock(&mut_sensor);

    aux = ganancia;

    pthread_mutex_unlock(&mut_sensor);

    return aux;
}
/*GETTER RESOLUCION*/
float sensor::get_Resolucion(){

    float aux;

    pthread_mutex_lock(&mut_sensor);

    aux= resolucion;

    pthread_mutex_unlock(&mut_sensor);

    return aux;
}
